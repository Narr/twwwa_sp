class View extends EventEmitter {
    constructor(model) {
        super("view");
        this._model = model;

        this.defaultLayout();
        this._model.pridejUdalost('refresh-view', () => this.listAllBookmarks());
    }

    defaultLayout() {
        let app_html = `
        <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <span class="navbar-brand">Bookmark Manager - ${this._model._baseURL.split("/a", 1)}</span>
                <button id="bt_addBookmark" type="button" class="float-right btn btn-primary">Přidat záložku</button>
            </div>
        </nav>
        <div class="container">
            <div id="page-content"></div>
        </div>`;

        $("#app").html(app_html);
        this.dataForm(document.getElementById("app"));
        document.getElementById("bt_addBookmark").addEventListener('click', () => this.insertBookmark());
    }

    listAllBookmarks() {
        //console.log("listing all items...");
        let itemsArr = this._model.itemsArr;
        let itemsHtml = ``;
        //console.log("view items arr",itemsArr);

        $("#page-content").empty();

        let itemsGroup = `
        <ul class='list-group list-group-flush'>
        </ul>
        `;

        $("#page-content").html(itemsGroup);

        itemsArr.forEach((val, index) => {
            let bt_span = document.createElement("span");
            bt_span.className = "glyphicon glyphicon-edit";
            bt_span.appendChild(document.createTextNode("Upravit"));

            let bt_update = document.createElement("button");
            bt_update.className = "btn btn-info mr-2";
            bt_update.appendChild(bt_span);
            bt_update.addEventListener('click', () => this.updateBookmark(val.id, index));

            let bt_span1 = document.createElement("span");
            bt_span1.className = "glyphicon glyphicon-remove";
            bt_span1.appendChild(document.createTextNode("Smazat"));

            let bt_del = document.createElement("button");
            bt_del.className = "btn btn-danger";
            bt_del.appendChild(bt_span1);
            bt_del.addEventListener('click', () => this.vykonejUdalost('delete', val.id));

            let itemContainer = document.createElement("div");
            itemContainer.className = "container";

            let row = document.createElement("div");
            row.className = "row";
            row.innerHTML = `
                <a href="${val.url}" class="col-sm-5"><span class="align-middle">${val.title}</span></a>
                <div class="col-sm-4"><span class="align-middle">${val.url}</span></div>
            `;

            let buttons_wrap = document.createElement("div");
            buttons_wrap.className = "col-sm-3 text-center";

            buttons_wrap.insertAdjacentElement('beforeend',bt_update);
            buttons_wrap.insertAdjacentElement('beforeend',bt_del);

            row.insertAdjacentElement('beforeend',buttons_wrap);
            itemContainer.insertAdjacentElement('beforeend',row);

            let li = document.createElement("li");
            li.appendChild(itemContainer);
            li.className = "list-group-item";

            document.querySelector(".list-group").insertAdjacentElement('beforeend',li);
        });
    }

    insertBookmark() {
        $("#modalForm").modal();
        $("#tb_submitForm").click(() => this.vykonejUdalost('insert'));
        //document.getElementById("tb_submitForm").addEventListener('click', () => this.vykonejUdalost('insert'));
    }

    updateBookmark(id, index) {
        $("#modalForm").modal();
        this.vykonejUdalost('getOne', index);
        $("#tb_submitForm").click(() => this.vykonejUdalost('update', id));
        //document.getElementById("tb_submitForm").addEventListener('click', () => this.vykonejUdalost('update', id));
    }

    dataForm(element) {
        let htmlForm = `
            <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header border-bottom-0">
                    <h5 class="modal-title" id="exampleModalLabel">Vytvořit záložku</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form id="data-form" action="javascript:void(0);">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputTitle">Název</label>
                            <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Název záložky" required>
                        </div>
                        <div class="form-group">
                            <label for="inputURL">URL adresa:</label>
                            <input type="url" class="form-control" id="inputURL" name="url" placeholder="Url záložky" required>
                        </div>  
                    </div>
                    <div class="modal-footer border-top-0 d-flex justify-content-center">
                      <button type="button" class="btn btn-primary" id="tb_submitForm">Uložit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        `;

        element.insertAdjacentHTML('beforeend',htmlForm);
    }

    static setupLayout() {
        let setupForm = `
            <div class="container">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header border-bottom-0">
                    <h5 class="modal-title" id="exampleModalLabel">První spuštění</h5>
                  </div>
                  <form id="setup-data-form" action="javascript:void(0);">
                    <div class="modal-body">
                        <p>Zadejte prosím URL adresu API s kterým má aplikace pracovat.</p>
                        <div class="form-group">
                            <label for="inputURL">URL adresa API:</label>
                            <input type="url" class="form-control" id="apiURL" name="url" placeholder="Url API (př.: http://example.com)" required>
                        </div>  
                    </div>
                    <div class="modal-footer border-top-0 d-flex justify-content-center">
                      <button type="button" class="btn btn-primary" id="setup_tb_submitForm">Uložit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        `;

        $("#app").html(setupForm);

        document.getElementById("setup_tb_submitForm").addEventListener('click', () => Controller.saveApiURL());
    }
}