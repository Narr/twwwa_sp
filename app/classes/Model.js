class Model extends EventEmitter {
    constructor(baseUrl = "http://localhost") {
        super('model');
        this._itemsArr = [];
        this._baseURL = baseUrl + "/api/bookmark/api.php";
    }

    set itemsArr(arr) {
        this._itemsArr = arr;
    }

    get itemsArr() {
        return this._itemsArr;
    }

    refreshState() {
        this.getAll();
    }

    getAll() {
        $.getJSON(this._baseURL + "?read", (data) => {
            this.itemsArr = data.records;
            console.log("Data z DB získány", this.itemsArr);
            this.vykonejUdalost("refresh-view");
        }).fail( (xhr, resp, text) => {
            this.itemsArr = [];
            this.vykonejUdalost("refresh-view");
            //console.log(xhr, resp, text);
        });
    }

    insertBookmark(data) {
        $.ajax({
            url: this._baseURL + "?create",
            type: "POST",
            contentType: "application/json",
            data: data,
            success: () => {
                this.refreshState();
                console.log("pridan zaznam do DB");
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        });
    }

    getOneBookmark(id) {
        $.ajax({
            url: this._baseURL + "?read_one",
            type: "GET",
            contentType: "application/json",
            data: {id: id},
            success: (res) => {
                this.itemsArr = res;
                console.log("ziskan 1 zaznam z DB");
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        });
    }

    updateBookmark(data) {
        $.ajax({
            url: this._baseURL + "?update",
            type: "POST",
            contentType: "application/json",
            data: data,
            success: () => {
                this.refreshState();
                console.log("upraven zaznam v DB");
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        });
    }

    deleteBookmark(id) {
        $.ajax({
            url: this._baseURL + "?delete",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({id: id}),
            success: () => {
                this.refreshState();
                console.log("smazan zaznam s id: " + id);
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        });
    }

    static getApiURL() {
        return localStorage.getItem('apiURL');
    }

    static setApiURL(url) {
        return localStorage.setItem('apiURL', url);
    }

}