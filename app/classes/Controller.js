class Controller {
    constructor(model, view) {
        this._model = model;
        this._view = view;

        this.index();
        this._view.pridejUdalost("delete", (id) => this.deleteBookmark(id));
        this._view.pridejUdalost("insert", () => this.insertBookmark());
        this._view.pridejUdalost("update", (id) => this.updateBookmark(id));
        this._view.pridejUdalost("getOne", (index) => this.getOneBookmark(index));
    }

    index() {
        this._model.getAll();
    }

    deleteBookmark(id) {
        this._model.deleteBookmark(id);
        console.log("Controller: delete proveden");
    }

    insertBookmark() {
        let form = document.getElementById("data-form");
        if(form.reportValidity()) {
            $("#tb_submitForm").off();
            //document.getElementById("tb_submitForm").removeEventListener('click', () => this.vykonejUdalost('insert'));
            let data = JSON.stringify($("#data-form").serializeObject());
            this._model.insertBookmark(data);
            console.log("Controller: insert proveden");
            form.reset();
            $('#modalForm').modal('hide');
        } else {
            console.log("Controller: insertBookmark - invalid data");
        }
    }

    updateBookmark(id) {
        let form = document.getElementById("data-form");
        if(form.reportValidity()) {
            $("#tb_submitForm").off();
            //document.getElementById("tb_submitForm").removeEventListener('click', () => this.vykonejUdalost('update', id));
            let dataObj = $("#data-form").serializeObject();
            dataObj.id = id;
            let data = JSON.stringify(dataObj);
            this._model.updateBookmark(data);
            console.log("Controller: update proveden");
            form.reset();
            $('#modalForm').modal('hide');
        } else {
            console.log("Controller: updateBookmark - invalid data");
        }
    }

    getOneBookmark(index) {
        $("#inputTitle").val(this._model.itemsArr[index].title);
        $("#inputURL").val(this._model.itemsArr[index].url);
        console.log("getOne item id: " + this._model.itemsArr[index].id);
        console.log("Controller: getOne proveden, index: " + index);
    }

    static saveApiURL() {
        let form = document.getElementById("setup-data-form");
        if(form.reportValidity()) {
            let apiUrl = document.getElementById("apiURL").value;
            Model.setApiURL(apiUrl);
            App.runApp(apiUrl);
        } else {
            console.log("Controller: saveApiURL - invalid data");
        }
    }
}