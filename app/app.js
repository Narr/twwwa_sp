window.addEventListener('load', function () {
    let apiUrl = Model.getApiURL();
    if(apiUrl === null) {
        View.setupLayout();
    } else {
        App.runApp(apiUrl);
    }
});

class App {
    static runApp(apiURL) {
        const model = new Model(apiURL);
        const view = new View(model);
        const contrl = new Controller(model, view);
    }
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};