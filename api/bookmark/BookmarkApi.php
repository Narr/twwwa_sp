<?php
require_once '../config/Database.php';
require_once '../objects/Bookmark.php';

class BookmarkApi
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function createBookmark() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $dbconn = $this->db->openConnection();

        $bookmark = new Bookmark($dbconn);

        $data = json_decode(file_get_contents("php://input"));

        if (!empty($data->title) && !empty($data->url)) {
            $bookmark->title = $data->title;
            $bookmark->url = $data->url;

            if($bookmark->insert()) {
                http_response_code(201);
                echo json_encode(
                    array("message" => "Záložka byla vytvořena.")
                );
            } else {
                http_response_code(503);
                echo json_encode(
                    array("message" => "Nelze vytvořit záložku.")
                );
            }
        } else {
            http_response_code(400);
            echo json_encode(
                array("message" => "Nelze vytvořit záložku. Chybná data.")
            );
        }
    }

    public function readBookmark() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");

        $dbconn = $this->db->openConnection();

        $bookmark = new Bookmark($dbconn);
        $stmt = $bookmark->getAll();
        $num = $stmt->rowCount();

        if ($num > 0) {
            $bookmark_arr = array();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $bookmark_arr["records"] = $stmt->fetchAll();

            http_response_code(200);

            echo json_encode($bookmark_arr);
        } else {
            http_response_code(404);
            echo json_encode(
                array("message" => "Záložky nebyly nalezeny.")
            );
        }
    }

    public function updateBookmark() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $dbconn = $this->db->openConnection();

        $bookmark = new Bookmark($dbconn);

        $data = json_decode(file_get_contents("php://input"));

        if (!empty($data->title) && !empty($data->url) && !empty($data->id)) {
            $bookmark->id = $data->id;
            $bookmark->title = $data->title;
            $bookmark->url = $data->url;

            if ($bookmark->update()) {
                http_response_code(200);
                echo json_encode(
                    array("message" => "Záložka byla upravena.")
                );
            } else {
                http_response_code(503);
                echo json_encode(
                    array("message" => "Nelze upravit záložku.")
                );
            }
        } else {
            http_response_code(400);
            echo json_encode(
                array("message" => "Nelze upravit záložku. Chybná data.")
            );
        }
    }

    public function deleteBookmark() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $dbconn = $this->db->openConnection();

        $bookmark = new Bookmark($dbconn);

        $data = json_decode(file_get_contents("php://input"));

        if (!empty($data->id)) {
            $bookmark->id = $data->id;
            if($bookmark->delete()) {
                http_response_code(200);
                echo json_encode(
                    array("message" => "Záložka byla odstraněna.")
                );
            } else {
                http_response_code(503);
                echo json_encode(
                    array("message" => "Nelze odstranit záložku.")
                );
            }
        } else {
            http_response_code(400);
            echo json_encode(
                array("message" => "Nelze odstranit záložku. Chybná data.")
            );
        }
    }

    public function readOneBookmark() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: access");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Allow-Credentials: true");
        header('Content-Type: application/json; charset=UTF-8');

        $dbconn = $this->db->openConnection();

        $bookmark = new Bookmark($dbconn);
        $bookmark->id = isset($_GET["id"]) ? $_GET["id"] : die();

        $bookmark->getOne();
        if ($bookmark->title != null) {
            $bookmark_arr = array(
                "id" => $bookmark->id,
                "title" => $bookmark->title,
                "url"  => $bookmark->url
            );

            http_response_code(200);
            echo json_encode($bookmark_arr);
        } else {
            http_response_code(404);

            echo json_encode(
                array("message" => "Záložka nebyla nalezena.")
            );
        }
    }
}