<?php

require_once "BookmarkApi.php";

if($_GET) {
    if(isset($_GET['create'])) {
        $api = new BookmarkApi();
        $api->createBookmark();
    } elseif(isset($_GET['read'])){
        $api = new BookmarkApi();
        $api->readBookmark();
    } elseif(isset($_GET['update'])) {
        $api = new BookmarkApi();
        $api->updateBookmark();
    } elseif (isset($_GET['delete'])) {
        $api = new BookmarkApi();
        $api->deleteBookmark();
    } elseif(isset($_GET['read_one'])){
        $api = new BookmarkApi();
        $api->readOneBookmark();
    }
}
