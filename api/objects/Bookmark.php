<?php


class Bookmark
{
    private $conn;

    public $id;
    public $title;
    public $url;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getAll() {
        $query = "SELECT * FROM `bookmark`";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function getOne() {
        $query = "SELECT * FROM `bookmark` b WHERE b.id = ? LIMIT 1";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$this->id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if($row !== false) {
            $this->title = $row["title"];
            $this->url = $row["url"];
        }
    }

    public function insert() {
        $this->title = htmlspecialchars(strip_tags($this->title));
        //TODO: možná sanitace url?
        $query = "INSERT INTO `bookmark` VALUES (NULL, :title, :url)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":url", $this->url);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    public function update() {
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->id = htmlspecialchars(strip_tags($this->id));

        $query = "UPDATE `bookmark` SET title = :title, url = :url WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":url", $this->url);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    public function delete() {
        $this->id = htmlspecialchars(strip_tags($this->id));

        $query = "DELETE FROM `bookmark` WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute([$this->id])) {
            return true;
        }

        return false;
    }
}